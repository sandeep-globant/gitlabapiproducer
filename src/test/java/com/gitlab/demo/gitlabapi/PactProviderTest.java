package com.gitlab.demo.gitlabapi;


import au.com.dius.pact.provider.junit5.HttpTestTarget;
import au.com.dius.pact.provider.junit5.PactVerificationContext;
import au.com.dius.pact.provider.junit5.PactVerificationInvocationContextProvider;
import au.com.dius.pact.provider.junitsupport.Provider;
import au.com.dius.pact.provider.junitsupport.State;
import au.com.dius.pact.provider.junitsupport.StateChangeAction;
import au.com.dius.pact.provider.junitsupport.loader.PactBroker;
import au.com.dius.pact.provider.junitsupport.loader.PactBrokerAuth;
import au.com.dius.pact.provider.junitsupport.loader.PactFolder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestTemplate;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

//@ExtendWith(Pact)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Provider("GitLabCatalogue")
//@PactFolder("pacts")

@PactBroker(url = "https://globant.pactflow.io/",
authentication = @PactBrokerAuth(token = "p2fvF2x6nEYFyXtFg-z4DA"))
public class PactProviderTest {

    @LocalServerPort
    public int port;

    //this method is for verify pact contract
    @TestTemplate
    @ExtendWith(PactVerificationInvocationContextProvider.class)
    public void pactVerificationTest(PactVerificationContext context) {//PactVerificationContext => this is the class to run pact Contract in junit
        context.verifyInteraction();

    }

    //this method is for setting the envionment port(my server)
    @BeforeEach
    public void setUp(PactVerificationContext context) {
        context.setTarget(new HttpTestTarget("localhost", port));
    }


    //this method is setup for our interacation
    @State(value = "members exist", action = StateChangeAction.SETUP)
    public void membersExistSetup() {

    }

    @State(value = "members exist", action = StateChangeAction.TEARDOWN)
    public void membersExistTeardown() {

    }

}
