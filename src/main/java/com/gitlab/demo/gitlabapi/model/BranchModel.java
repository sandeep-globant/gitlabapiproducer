package com.gitlab.demo.gitlabapi.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BranchModel {

    private Integer projectId;
    private String branchName;
    private String branchRef;

    public BranchModel() {
    }

    public BranchModel(Integer projectId, String branchName, String branchRef) {
        this.projectId = projectId;
        this.branchName = branchName;
        this.branchRef = branchRef;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBranchRef() {
        return branchRef;
    }

    public void setBranchRef(String branchRef) {
        this.branchRef = branchRef;
    }

    @Override
    public String toString() {
        return "BranchModel{" +
                "projectId=" + projectId +
                ", branchName='" + branchName + '\'' +
                ", branchRef='" + branchRef + '\'' +
                '}';
    }
}
